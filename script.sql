create sequence FILEID
  nocache
/

create sequence FILETYPEID
  nocache
/

create sequence USERID
  nocache
/

create sequence ROLEID
  nocache
/

create sequence USERROLEID
  nocache
/

create sequence CONFIGID
  nocache
/

create sequence FILEACCESSLOGID
  nocache
/

create sequence FILEACCESSTOKENID
  nocache
/

create table CONFIG
(
  ID       NUMBER        not null,
  ROOT_DIR VARCHAR2(255) not null,
  ACTIVE   NUMBER(1)     not null,
  primary key (ID)
)
/

create trigger CONFIGTRIGGER
  before insert
  on CONFIG
  for each row
  BEGIN
    SELECT ConfigID.nextval
        INTO :new.id FROM dual;
  end;
/

create table FILE_TYPE
(
  ID   NUMBER       not null,
  TYPE VARCHAR2(50) not null,
  primary key (ID),
  check (type in ('file', 'directory'))
)
/

create trigger FILETYPETRIGGER
  before insert
  on FILE_TYPE
  for each row
  BEGIN
    SELECT FileTypeID.nextval
        INTO :new.id FROM dual;
  end;
/

create table ROLE
(
  ID   NUMBER        not null,
  NAME VARCHAR2(255) not null,
  primary key (ID)
)
/

create trigger ROLETRIGGER
  before insert
  on ROLE
  for each row
  BEGIN
    SELECT RoleID.nextval
        INTO :new.id FROM dual;
  end;
/

create table PERSON
(
  ID       NUMBER        not null,
  USERNAME VARCHAR2(255) not null,
  PASSWORD VARCHAR2(255) not null,
  primary key (ID)
)
/

create trigger USERTRIGGER
  before insert
  on PERSON
  for each row
  BEGIN
    SELECT UserID.nextval
        INTO :new.id FROM dual;
  end;
/

create table USER_ROLE
(
  ID      NUMBER not null,
  USER_ID NUMBER not null,
  ROLE_ID NUMBER not null,
  primary key (ID),
  constraint ROLE_ID_FK
  foreign key (ROLE_ID) references ROLE,
  constraint USER_ID_FK
  foreign key (USER_ID) references PERSON
)
/

create trigger USERROLETRIGGER
  before insert
  on USER_ROLE
  for each row
  BEGIN
    SELECT UserRoleID.nextval
        INTO :new.id FROM dual;
  end;
/

create table FMS_FILE
(
  ID           NUMBER        not null,
  NAME         VARCHAR2(255) not null,
  FILE_SIZE    NUMBER(19)    not null,
  IS_DELETED   NUMBER(1)     not null,
  CREATED_AT   TIMESTAMP(6)  not null,
  EXPIRY_DATE  TIMESTAMP(6),
  CREATED_BY   NUMBER        not null,
  PARENT_ID    NUMBER,
  FILE_TYPE_ID NUMBER        not null,
  primary key (ID),
  constraint CREATED_BY_FK
  foreign key (CREATED_BY) references PERSON,
  constraint FILE_TYPE_ID_FK
  foreign key (FILE_TYPE_ID) references FILE_TYPE,
  constraint PARENT_ID_FK
  foreign key (PARENT_ID) references FMS_FILE
)
/

create trigger FILETRIGGER
  before insert
  on FMS_FILE
  for each row
  BEGIN
    SELECT FileID.nextval
        INTO :new.id FROM dual;
  end;
/

create table FILE_ACCESS_LOG
(
  ID          NUMBER       not null,
  OPERATION   VARCHAR2(50) not null,
  ACCESS_TIME TIMESTAMP(6) not null,
  FILE_ID     NUMBER       not null,
  USER_ID     NUMBER       not null,
  primary key (ID),
  constraint FILE_ID_FK
  foreign key (FILE_ID) references FMS_FILE,
  constraint FILE_USER_ID_FK
  foreign key (USER_ID) references PERSON,
  check (operation in ('r', 'w'))
)
/

create trigger FILEACCESSLOGTRIGGER
  before insert
  on FILE_ACCESS_LOG
  for each row
  BEGIN
    SELECT FileAccessLogID.nextval
        INTO :new.id FROM dual;
  end;
/

create table FILE_ACCESS_TOKEN
(
  ID              NUMBER        not null,
  TOKEN           VARCHAR2(255) not null,
  EXPIRY_DATE     TIMESTAMP(6),
  NUM_READS       NUMBER,
  NUM_WRITES      NUMBER,
  NUM_READS_LEFT  NUMBER,
  NUM_WRITES_LEFT NUMBER,
  FILE_ID         NUMBER        not null,
  primary key (ID),
  constraint TOKEN_FILE_ID_FK
  foreign key (FILE_ID) references FMS_FILE
)
/

create trigger FILEACCESSTOKENTRIGGER
  before insert
  on FILE_ACCESS_TOKEN
  for each row
  BEGIN
    SELECT FileAccessTokenID.nextval
        INTO :new.id FROM dual;
  end;
/


